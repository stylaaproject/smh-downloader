<html>
  <head>
  	<title>Social Media Hacks Converter and download audio and videos</title>
      <meta name="description" content= "Download and convert youtube and facebook videos to various format including mp4 m4a wav mkv avi" />
      <meta name="robots" content= "index, follow">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script>
$(document).ready( function () {
  $("#div_download").hide();
  $(".container").hide();

$("#mybutton").click(function(e){ 
//$("#mybutton1").submit(function(e) {
    //alert('wish');
    e.preventDefault(); // prevent actual form submit
    var form = $('#video_form');
    var url = form.attr('action'); //get submit url [replace url here if desired]
    $("#mybutton").attr('disabled',true);
    // $("#recode_video").attr('disabled',true);
    // $("#youtube_dl").attr('disabled',true);
    $.ajax({
        type: "POST",
        url: url,
        data: form.serialize(), // serializes form input
        success: function(data){
        //alert(data);
        if(data != ""){

            if(data == "success"){
                $("#mybutton").attr('disabled','disabled');
                $("#recode_video").attr('disabled','disabled');
                $("#youtube_dl").attr('disabled','disabled');
                $("#sub_info").html("Processing your request, be patient if your file is big &nbsp; <img id='bar' src='./images/spinner.gif' />"); 
                $("#bar").show();
                $(".container").show(); 
                $("#div_download").hide();
            }else if(data == "missing"){
                $('#sub_info').text('Please enter a valid youtube URL');
                $("#mybutton").attr('disabled',false);
                $("#recode_video").attr('disabled',false);
                $("#youtube_dl").attr('disabled',false);
                $('#bar').hide();
                $(".container").hide();
            }else if(data == "error-file"){
                $('#sub_info').text('Was not able to find task, try again');
                $("#mybutton").attr('disabled',false);
                $("#recode_video").attr('disabled',false);
                $("#youtube_dl").attr('disabled',false);
                $('#bar').hide();
                $(".container").hide();
            }else if(data == "error-task"){
                $('#sub_info').text('Was not able to create task, refresh page and try again');
                $("#mybutton").attr('disabled',false);
                $("#recode_video").attr('disabled',false);
                $("#youtube_dl").attr('disabled',false);
                $('#bar').hide();
                $(".container").hide();
            }else if(data == "error-task1"){
                $('#sub_info').text('Was not able to create download task, try again');
                $("#mybutton").attr('disabled',false);
                $("#recode_video").attr('disabled',false);
                $("#youtube_dl").attr('disabled',false);
                $('#bar').hide();
                $(".container").hide();
            }else if( /^File/.test(data) ){
            //}else if(data == "error-size"){
                alert(data);
                $('#sub_info').html('This media is over the required size. <a href=/>Try Again</a>');
                //$("#mybutton").attr('disabled',false);
                //$("#recode_video").attr('disabled',false);
                //$("#youtube_dl").attr('disabled',false);
                $("#mybutton").hide();
                //$("#recode_video").hide();
                $(".mydrop").hide();
                $("#youtube_dl").hide();
                $('#bar').hide();
                $(".container").hide();
            }else if( /^Conversion/.test(data) ){
            //}else if(data == "error-size"){
                alert(data);
                $('#sub_info').html('Your choosen fromat is not supported by the media you want to convert.  <a href=/>Try Again</a>');
                //$("#mybutton").attr('disabled',false);
                //$("#recode_video").attr('disabled',false);
                //$("#youtube_dl").attr('disabled',false);
                $("#mybutton").hide();
                //$("#recode_video").hide();
                $(".mydrop").hide();
                $("#youtube_dl").hide();
                $('#bar').hide();
                $(".container").hide();
            }else{
                $('#sub_info').text('Unknown error occured, try again later');
                $("#mybutton").attr('disabled',false);
                $("#recode_video").attr('disabled',false);
                $("#youtube_dl").attr('disabled',false);
                $('#bar').hide();
                $(".container").hide();              
            }
            
          }else{
                $('#sub_info').text('No response from server. Are you online, try again');
                $("#mybutton").attr('disabled',false);
                $("#recode_video").attr('disabled',false);
                $("#youtube_dl").attr('disabled',false);
                $('#bar').hide();
                $(".container").hide(); 
          }
        }
    });
});

});
</script>
<link rel="stylesheet" href="./css/site.css"/>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.4.0/css/bootstrap.min.css"/>
</head>
<body>
<?php

$active = "Video";
$linked = "Audio";

$youtube_dl = $_REQUEST["youtube_dl"];
$format = $_REQUEST["format"];
$recode_video = $_REQUEST["recode_video"];
$audio_format = $_REQUEST["audio_format"];
$rand = rand();
$youtube_dl = explode('=', $youtube_dl);
$youtube_dl =  $youtube_dl[1];
$folder = "./files/";
$download_folder = "/files/downloads/";
#$output4 = shell_exec('rm -rf ./files/*');
$config = $folder . $rand . '/config ';
$rand_log = $folder . $rand . "/". $rand . ".txt";
#$download_link = $download_folder. $rand . ".tar.gz";

$download_link = "./downloads.php?filename=$rand.tar.gz";
#echo $rand;

?>
<script type="text/javascript">
    setInterval(function () {
        $.ajax({
            url: 'progress.php?rand=<?php echo $rand; ?>',
            success: function (data) {

                if(data > 0){
                   $('#progress-string').html(`${data}%`);
                   $('#progressbar').attr('aria-valuenow', data).css('width', `${data}%`);
                }else{
                   if(data == "Converting"){
                       $('#progress-string').html('Converting to choosen format ...');
                       $('#progressbar').attr('aria-valuenow', data).css('width', '100%');
                   }else if (data == "Merging"){
                       $('#progress-string').html('Merging files ...');
                       $('#progressbar').attr('aria-valuenow', data).css('width', '100%');
                   }else if (data == "Compressing"){
                       $('#progress-string').html('Compressing files ...');
                       $('#progressbar').attr('aria-valuenow', data).css('width', '100%');
                   }else if (data == "Cleanup"){
                       $('#progress-string').html('Cleanup ...');
                       $('#progressbar').attr('aria-valuenow', data).css('width', '100%');
                   }else if (data == "Complete"){
                       $('#progress-string').html('Complete');
                       $('#progressbar').attr('aria-valuenow', data).css('width', '100%');
                       $(".container").hide();
                       $('#div_form').hide();
                       $('#bar').hide();                   
                       $("#div_download").show();
                       $('#mydownload').attr('href', '<?php echo $download_link ?>');
                       $('#sub_info').html('Click below to download your file. Download link will expire after 10 minutes');
                   }
               }
            }
        });
    }, 500);

</script>
 <div name="" id="Menu">
  <div class="submenu"><h6><?php echo $active ?> Converter</h6></div>
  <div class="submenu"><h6><a href=/audio.php ><?php echo $linked ?> Converter</a></h6></div>
</div>
 <div id="ad_top"></div>
<div class="form_center" id="div_heading">
   
<div class="formcase"  align="center">
  <h2 id="h2_title"> <?php echo $active ?> Converter 2.0</h2>
  <span id="sub_info">Enter youtube URL below and choose a format </span>
 

<div class="container" id="container">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="progress">
                    <div id="progressbar" class="progress-bar bg-info" role="progressbar" aria-valuenow="0"
                         aria-valuemin="0" aria-valuemax="100"><span id="progress-string"></span></div>
                </div>
            </div>
        </div>
    </div>
</div>


</div>
<div class="formcase" id="div_form">
  <form id="video_form" name="video_form" method="POST" action="process.php" target="my_target" align="center">
    <input class="mytext" type="text" name="youtube_dl" id="youtube_dl"   />
    <input type="hidden"  value="" name="format" id="format">
    <input type="hidden"  value="" name="audio_format" id="audio_format">
    <input type="hidden"  value="<?php echo $rand; ?>" name="rand" id="rand">
    <select name="recode_video" id="recode_video" class="mydrop">
       <option value="">best</option>
       <option value="mp4">mp4</option>
       <option value="flv">flv</option>
       <option value="avi">avi</option>
       <option value="ogg">ogg</option>
       <!--<option value="mkv">mkv</option>-->
       <option value="webm">webm</option>
    </select>
    <button id="mybutton" name="mybutton" class="mybutton" >Process</button>
  </form>
</div>
<div class="formcase"  align="center" id="div_download">
<a id="mydownload" name="mydownload" class="mybutton"  >Download</a>
<a id="mystart" name="mystart" class="mybutton" href="/" >Start Again</a>
</div>
</div>
<div id="ad_base"></div>
<div id="footer"><a href=#>About</a> &nbsp;&nbsp; | &nbsp;&nbsp;<a href=#>Contact</a>&nbsp;&nbsp; | &nbsp;&nbsp;<a href=#>Premium</a></div>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-180565359-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-180565359-1');
</script>

  </body>
</html>

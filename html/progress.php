<?php
$log_file_path = "./files/logs/" . $_REQUEST['rand'] . ".log";

function lineAsArray(string $filepath): array
{
    $line = '';
    $f = fopen($filepath, 'r');
    $cursor = -1;
    fseek($f, $cursor, SEEK_END);
    $char = fgetc($f);
    while ($char === "\n" || $char === "\r") {
        fseek($f, $cursor--, SEEK_END);
        $char = fgetc($f);
    }
    while ($char !== false && $char !== "\n" && $char !== "\r") {
        $line = $char . $line;
        fseek($f, $cursor--, SEEK_END);
        $char = fgetc($f);
    }
    $remove_whitespace = preg_replace('/\s+/', ' ', $line);
    return explode(" ", $remove_whitespace);
}
function progress(array $array): string
{
    return str_replace('%', '', $array[1]);
}
$last_line = lineAsArray($log_file_path);
echo progress($last_line);


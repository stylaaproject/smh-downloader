#!/bin/bash

# Using single quotes instead of double quotes to make it work with special-character passwords
PROJECTFOLDER='html'
SERVICE_PRJ='cron'

USER_WEB="www-data"
USER_SERVICE="ubuntu"


echo "stop nginx"
sudo service nginx stop

echo "remove last push"
sudo rm -rf /etc/nginx/sites-available/
sudo rm -rf ./smh-downloader

echo "remove nginx"
sudo apt-get remove nginx nginx-common
sudo apt-get purge nginx nginx-common
sudo apt-get autoremove

# #remove www folder
# echo "Removing web and cron files"
# sudo rm -rf /var/www

sudo rm --rf /var/www

# create project folder
sudo mkdir -p "/var/www/${PROJECTFOLDER}"

# update / upgrade
echo "Updating apt-get..."
sudo apt-get update #> /dev/null 2>&1
#sudo apt-get -y upgrade #> /dev/null 2>&1

# install nginx
echo "Installing Nginx..."
sudo apt-get install -y git nginx #> /dev/null 2>&1

# install php7-fpm
echo "Installing PHP..."
sudo apt-get install -y php-fpm php-mysql php-xml php-gd php7.0-zip #> /dev/null 2>&1

#install mariadb and give password to installer
#echo "Preparing MariaDB..."
#sudo apt-get install -y debconf-utils #> /dev/null 2>&1
#sudo debconf-set-selections <<< "mysql-server mysql-server/root_password password $PASSWORD"
#sudo debconf-set-selections <<< "mysql-server mysql-server/root_password_again password $PASSWORD"
#sudo apt-key adv --recv-keys --keyserver hkp://keyserver.ubuntu.com:80 0xF1656F24C74CD1D8
#sudo add-apt-repository 'deb [arch=amd64,i386] http://sfo1.mirrors.digitalocean.com/mariadb/repo/10.3/ubuntu xenial main'

echo "Updating apt-get..."
sudo apt-get update #> /dev/null 2>&1
 
#echo "Installing MariaDB..."
#sudo apt-get install -y mariadb-server

#create www-data user
echo "Create user service/web"
sudo chown -R ${USER_SERVICE}:${USER_WEB} /var/www

echo "Get package from git ..."
git clone https://stylaaproject@bitbucket.org/stylaaproject/smh-downloader.git

# Nginx Config
echo "Configuring Nginx ..."
sudo cp ./smh-downloader/nginx/nginx_vhost /etc/nginx/sites-available/ #> /dev/null 2>&1
sudo ln -s /etc/nginx/sites-available/nginx_vhost /etc/nginx/sites-enabled/
sudo rm -rf /etc/nginx/sites-enabled/default
echo "Remove unused file ..."
sudo rm -rf /var/www/html/index.nginx-debian.html
#git pull origin master



ls ./smh-downloader -alt

echo "Setting up website files ..."
sudo cp -rf ./smh-downloader/${PROJECTFOLDER} /var/www/ 
sudo chown -R ${USER_WEB}:${USER_WEB} /var/www/${PROJECTFOLDER}

sudo cp -rf ./smh-downloader/${SERVICE_PRJ} /var/www/ 
sudo chown -R ${USER_SERVICE}:${USER_SERVICE} /var/www/${SERVICE_PRJ}
#sudo mkdir /var/www/html/files/files && sudo -R chown www-data:www-data /var/www/html




echo "Install Youtube dl ..."
#sudo apt-get install -y youtube-dl
#sudo curl -L https://yt-dl.org/downloads/latest/youtube-dl -o /usr/local/bin/youtube-dl
sudo cp -rf ./smh-downloader/config/smh /usr/bin/
#sudo ln -s /usr/bin/smh /var/usr/local/bin/ 
sudo chmod a+rx /usr/bin/smh

echo "Install ffmpeg zip unzip jq ksh ..."
sudo apt-get install -y ffmpeg zip unzip jq ksh

# Restarting Nginx for config to take effect
echo "Restarting Nginx ..."
sudo service nginx restart #> /dev/null 2>&1

echo "install Crontab ..."
sudo echo "

#Cron jobs for processing

# everyday at 7am
0  7    * * *   root  sh  /var/www/cron/_clear_d.sh >> /var/www/cron/logs/_d.log 2>&1

# everyday at 7pm
0  19    * * *  root  sh  /var/www/cron/_clear_d.sh >> /var/www/cron/logs/_d.log 2>&1

*  *    * * *   root    sh /var/www/cron/_cron.sh cron_1 >> /var/www/cron/logs/_queue_1.log 2>&1
*  *    * * *   root    sleep 5 && sh /var/www/cron/_cron.sh cron_2 >> /var/www/cron/logs/_queue_2.log 2>&1
*  *    * * *   root    sleep 10 && sh /var/www/cron/_cron.sh cron_3 >> /var/www/cron/logs/_queue_3.log 2>&1
*  *    * * *   root    sleep 15 && sh /var/www/cron/_cron.sh cron_4 >> /var/www/cron/logs/_queue_4.log 2>&1
*  *    * * *   root    sleep 20 && sh /var/www/cron/_cron.sh cron_5 >> /var/www/cron/logs/_queue_5.log 2>&1
*  *    * * *   root    sleep 25 && sh /var/www/cron/_cron.sh cron_6 >> /var/www/cron/logs/_queue_6.log 2>&1
*  *    * * *   root    sleep 30 && sh /var/www/cron/_cron.sh cron_7 >> /var/www/cron/logs/_queue_7.log 2>&1
*  *    * * *   root    sleep 35 && sh /var/www/cron/_cron.sh cron_8 >> /var/www/cron/logs/_queue_8.log 2>&1
*  *    * * *   root    sleep 40 && sh /var/www/cron/_cron.sh cron_9 >> /var/www/cron/logs/_queue_9.log 2>&1
*  *    * * *   root    sleep 45 && sh /var/www/cron/_cron.sh cron_10 >> /var/www/cron/logs/_queue_10.log 2>&1
*  *    * * *   root    sleep 50 && sh /var/www/cron/_cron.sh cron_11 >> /var/www/cron/logs/_queue_11.log 2>&1
*  *    * * *   root    sleep 55 && sh /var/www/cron/_cron.sh cron_12 >> /var/www/cron/logs/_queue_12.log 2>&1


" >> /etc/crontab

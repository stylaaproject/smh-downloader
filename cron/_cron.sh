#!/bin/ksh

process_dir=/var/www/cron/process
complete_dir=/var/www/cron/complete
download_dir=/var/www/html/files/downloads
logs_dir=/var/www/html/files/logs
smh=youtube-dl

echo "Process $1 checking ..."
job=$1
runfile=/var/www/cron/is_running/$job

echo "Running task : " $runfile

#check the latest file to process
file=$( find /var/www/html/files/files/ -name *.txt -type f -printf '%p\n' | sort | head -n 1 )
#file=$( find /var/www/html/files/files/ -type f -printf '%p\n' | sort | head -n 1 )

#just checking for errors
if [ $? -eq 0 ]; then
    echo "dir accessible OK (/var/www/html/files/files/)"
else
    echo "An error occured"
    exit 0
fi


#Check if ready to process files exists
if [  -z "$file" ]; then
    echo "No file to process"
    exit 0 
else

    if [[ -f "$runfile" ]]; then
        echo "A job : $job is currently running on this service. Check back later"
        exit 0
    else
        echo "Started process task with : $job" > $runfile
        echo $runfile
    fi
fi

echo "file is " $file


#basename "$file"
filename="$(basename -- $file)"
echo $filename

foldername=${filename%.*}

echo $foldername

#Make directory
sudo mkdir $process_dir/$foldername

#move the file
sudo mv $file $process_dir/$foldername/$filename

#echo file in process folder
process_file=$(ls -1 $process_dir/$foldername)
echo "file to be processed: " $process_file
#read process file

id=$(sed -n '1,1p' $process_dir/$foldername/$filename)
youtube=$(sed -n '2,1p' $process_dir/$foldername/$filename)
format=$(sed -n '3,1p' $process_dir/$foldername/$filename)
recode_video=$(sed -n '4,1p' $process_dir/$foldername/$filename)
audio_format=$(sed -n '5,1p' $process_dir/$foldername/$filename)

echo $id
echo $youtube
echo $format
echo $recode_video
echo $audio_format

echo "           "

if [ -z "${format}" ]; then
   format_new=""
   #echo empty
else
   format_new=$format
   #echo "not empty"
fi
echo "This is format " $format
echo "This is new format " $format_new


if [ -z "${audio_format}" ]; then
   audio_format_new=""
else
   audio_format_new="--audio-format  $audio_format "
   #get best selection if not get best available
   #audio_format_new="-f 'bestaudio[ext=m4a]/best[ext=$audio_format]/best' "
fi

echo "This is audio_format " $audio_format
echo "This is audio_format_new " $audio_format_new



if [ -z "${recode_video}" ]; then 
   recode_video_new=""
else
   recode_video_new="--recode-video  $recode_video "  
fi

echo "This is recode_video " $recode_video
echo "This is recode_video_new " $recode_video_new

echo "-o $process_dir/$foldername/%(id)s.%(ext)s" > $process_dir/$foldername/config

config=$process_dir/$foldername/config
rand_log=$logs_dir/$foldername.log

echo $config
echo $rand_log

smh --version

echo "smh $youtube $format_new $audio_format_new $recode_video_new --config-location $config --newline --geo-bypass --console-title - >  $rand_log;"

#-o "%(id)s.%(ext)s" 

#sleep 2
#--simulate
#--max-filesize 1MB 1GB
smh $audio_format_new $youtube $format_new  $recode_video_new --config-location  $config  --newline --geo-bypass --console-title  >>  $rand_log;
#youtube-dl https://www.facebook.com/watch/?v=393181355044758

#youtube-dl  $youtube  

echo "[downloads] Compressing of XXXXXMiB at  XXXXXMiB/s ETA 00:00"  >> $rand_log;

cd $process_dir && tar -czvf $foldername.tar.gz $foldername/  --exclude $foldername/$filename --exclude $rand_log --exclude $foldername/config


if [ $? -eq 0 ]; then
    echo "File zipped successfully"
else
    echo "An error occured"
    exit 0
fi

echo "mv ./$foldername.tar.gz to $download_dir"
sudo cp -rf ./$foldername.tar.gz $download_dir
sudo rm ./$foldername.tar.gz

echo "[downloads] Complete of XXXXXMiB at  XXXXXMiB/s ETA 00:00"  >> $rand_log;

sudo rm $process_dir/$foldername/$filename

#this file is used to load the loading bar and should be removed after download button appears by a cron job
#sudo mv  $rand_log $complete_dir

#remove process files
sudo rm -rf $process_dir/$foldername
#remove runnign task so next task can run
#rm /var/www/cron/is_running/running
sudo rm $runfile

exit 0


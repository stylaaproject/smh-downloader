#!/bin/ksh

download_dir=/var/www/html/files/downloads/*.gz
logs_dir=/var/www/html/files/logs/*.log

find $download_dir -mtime +1 -delete

if [ $? -eq 0 ]; then
    echo "cleared download folder ( $download_dir )"
else
    echo "An error occured"
fi

find $logs_dir -mtime +1 -delete

if [ $? -eq 0 ]; then
    echo "cleared logs folder ( $logs_dir )"
else
    echo "An error occured"
fi

exit 0
